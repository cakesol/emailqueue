<?php
// In ContactManager plugin bootstrap.php
use Cake\Event\EventManager;
use Cakesol\Emailqueue\Event\EmailListener;

$listener = new EmailListener();
EventManager::instance()->attach($listener);