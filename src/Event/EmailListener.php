<?php
/**
 * Created by PhpStorm.
 * User: remigius
 * Date: 6-2-2018
 * Time: 10:39
 */

namespace Cakesol\Emailqueue\Event;

use Cake\Event\Event;
use Cake\Event\EventListenerInterface;


class EmailListener implements EventListenerInterface {

	/**
	 * @return array
	 */
    public function implementedEvents() {
        return [
	        'Model.initialize' => 'extendMailer'
        ];
    }

	/**
	 * @param Event $event
	 */
    public function extendMailer(Event $event) {
    	$a = '';
    }
}