<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * EmailQueue Model
 *
 * @method \App\Model\Entity\EmailQueue get($primaryKey, $options = [])
 * @method \App\Model\Entity\EmailQueue newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\EmailQueue[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\EmailQueue|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\EmailQueue|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\EmailQueue patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\EmailQueue[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\EmailQueue findOrCreate($search, callable $callback = null, $options = [])
 */
class EmailQueueTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('email_queue');
        $this->setDisplayField('id_email_queue');
        $this->setPrimaryKey('id_email_queue');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id_email_queue')
            ->allowEmpty('id_email_queue', 'create');

        $validator
            ->scalar('unique_hash')
            ->maxLength('unique_hash', 32)
            ->requirePresence('unique_hash', 'create')
            ->notEmpty('unique_hash');

        $validator
            ->scalar('from')
            ->maxLength('from', 255)
            ->requirePresence('from', 'create')
            ->notEmpty('from');

        $validator
            ->scalar('from_name')
            ->maxLength('from_name', 255)
            ->allowEmpty('from_name');

        $validator
            ->scalar('to')
            ->maxLength('to', 255)
            ->requirePresence('to', 'create')
            ->notEmpty('to');

        $validator
            ->scalar('to_name')
            ->maxLength('to_name', 255)
            ->allowEmpty('to_name');

        $validator
            ->scalar('subject')
            ->maxLength('subject', 255)
            ->requirePresence('subject', 'create')
            ->notEmpty('subject');

        $validator
            ->scalar('body')
            ->requirePresence('body', 'create')
            ->notEmpty('body');

        $validator
            ->scalar('template')
            ->maxLength('template', 255)
            ->allowEmpty('template');

        $validator
            ->boolean('sent')
            ->requirePresence('sent', 'create')
            ->notEmpty('sent');

        $validator
            ->dateTime('date_add')
            ->requirePresence('date_add', 'create')
            ->notEmpty('date_add');

        $validator
            ->dateTime('date_sent')
            ->allowEmpty('date_sent');

        return $validator;
    }
}
