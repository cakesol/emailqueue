<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * EmailQueue Entity
 *
 * @property int $id_email_queue
 * @property string $unique_hash
 * @property string $from
 * @property string $from_name
 * @property string $to
 * @property string $to_name
 * @property string $subject
 * @property string $body
 * @property string $template
 * @property bool $sent
 * @property \Cake\I18n\FrozenTime $date_add
 * @property \Cake\I18n\FrozenTime $date_sent
 */
class EmailQueue extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'unique_hash' => true,
        'from' => true,
        'from_name' => true,
        'to' => true,
        'to_name' => true,
        'subject' => true,
        'body' => true,
        'template' => true,
        'sent' => true,
        'date_add' => true,
        'date_sent' => true
    ];
}
