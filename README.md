Configuration plugin for CakePHP
================================

Requirements
------------
* CakePHP 3.6.0+
* PHP 7.2+

Installation
------------
You can install this plugin into your CakePHP application using [composer](http://getcomposer.org).
The recommended way to install composer packages is:

```
composer require cakesol/config
```

or update your composer.json file as below
```
 "require": {
        "cakesol/config": "dev-master"
    },
```
Enable the plugin in your application bootstrap.php
```
Plugin::load('Cakesol/Config', ['bootstrap' => true]);
```

Database
--------

This plugin requires a migration to generate a `configs` table, and it
can be generated via the official Migrations plugin as follows:

```shell
bin/cake migrations migrate -p cakesol/config
```
Or you can use [this plugin](https://gitlab.com/cakesol/migration-panel) to manage the migrations via the admin

Support
-------

Please report bugs and feature request in the [issues](https://gitlab.com/cakesol/config/issues) section of this repository.


License
-------

Copyright 2018 Solutia. All rights reserved.

Licensed under the [MIT](http://www.opensource.org/licenses/mit-license.php) License. Redistributions of the source code included in this repository must retain the copyright notice found in each file.